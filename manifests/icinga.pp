class mobi_mediamuxer::icinga {

  if $::mobi_mediamuxer::icinga {
    if ! defined("icinga::register") {
      fail("icinga set to true in $name but icinga::register not declared (base class manage_icinga).")
    }

    if ! $::mobi_mediamuxer::icinga_instance {
      fail("Must provide icinga_instance parameter to mobi_mediamuxer module when icinga = true")
    }

    @@nagios_service { "check_http_mobi_mediamuxer_${fqdn}":
      host_name             => $::fqdn,
      check_command         => "check_http! ${::mobi_mediamuxer::icinga_cmd_args}",
      service_description => "check_http_mobi_mediamuxer",
      notes => "PATH: ${::mobi_mediamuxer::icinga_cmd_args}",
      normal_check_interval => "15", # mildly aggressive
      use                   => "generic-service",
      tag                   => "icinga_instance:${::mobi_mediamuxer::icinga_instance}",
      target                => "/etc/icinga/conf.d/${::fqdn}.cfg",
      notify                => Exec["icinga_reload"],
            require               => Class["icinga::register"],
    }

  }
}

